/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.persona.ec.bo;

import com.persona.ec.entity.persona;
import com.persona.ec.dao.personaDao;
import com.persona.ec.db.Conexion;
import java.sql.*;
import javax.swing.JTable;

/**
 *
 * @author werne
 */
public class personaBO {

    private String mensaje = "";
    private personaDao pdao = new personaDao();

    public String agregarPersona(persona per) {
        Connection conn= Conexion.getConnection();
        try{
            mensaje = pdao.agregarPersona(conn, per);
            
        }catch(Exception e){
            mensaje=mensaje+" "+e.getMessage();
        }finally{
            try{
                if(conn!= null){
                    conn.close();
                }
            }catch(Exception e){
                 mensaje=mensaje+" "+e.getMessage();
            }
        }
        return mensaje;
    }

    public String modificarPersona(persona per) {
    Connection conn= Conexion.getConnection();
        try{
            mensaje = pdao.modificarPersona(conn, per);
            
        }catch(Exception e){
            mensaje=mensaje+" "+e.getMessage();
        }finally{
            try{
                if(conn!= null){
                    conn.close();
                }
            }catch(Exception e){
                 mensaje=mensaje+" "+e.getMessage();
            }
        }
        return mensaje;
    }

    public String eliminarPersona(int id) {
       Connection conn= Conexion.getConnection();
        try{
            mensaje = pdao.eliminarPersona(conn, id);
            
        }catch(Exception e){
            mensaje=mensaje+" "+e.getMessage();
        }finally{
            try{
                if(conn!= null){
                    conn.close();
                }
            }catch(Exception e){
                 mensaje=mensaje+" "+e.getMessage();
            }
        }
        return mensaje;
    }

    public void listarPersona(JTable tabla) {
        Connection conn = Conexion.getConnection();
     pdao.listarPersona(conn, tabla);
     try{
            conn.close();
     }catch(SQLException ex){
         System.out.println(ex.getMessage());
     }
     
  
    }
}
