package com.persona.ec.entity;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author werne
 */
public class persona {
   
  
private int idPersona;
private String nombres;
private String correo;
private String telefono;

    public persona() {
    }

    public persona(int idPersona, String nombres, String correo, String telefono) {
        this.idPersona = idPersona;
        this.nombres = nombres;
        this.correo = correo;
        this.telefono = telefono;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return "persona{" + "idPersona=" + idPersona + ", nombres=" + nombres + ", correo=" + correo + ", telefono=" + telefono + '}';
    }


}
