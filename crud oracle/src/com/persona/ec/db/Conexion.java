/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.persona.ec.db;

import java.sql.*;
import javax.swing.JOptionPane;

/**
 *
 * @author werne
 */
public class Conexion {

    private static Connection conn = null;
    private static String user = "system";
    private static String pass = "oracle";
    private static String url = "jdbc:oracle:thin:@localhost:1521:xe";

    public static Connection getConnection() {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            conn = DriverManager.getConnection(url, user, pass);
            conn.setAutoCommit(false);
            if (conn != null) {
                System.out.println("Conexión Exitosa");
            } else {
                System.out.println("Conexión Fallida");
            }
        } catch (ClassNotFoundException | SQLException e) {
            JOptionPane.showMessageDialog(null, "Conexión erronea" + e.getMessage());
        }

        return conn;
    }

    public void desconexion() {
        try {
            conn.close();

        } catch (Exception e) {
            System.out.println("Error Al desconectar" + e.getMessage());
        }
    }

    public static void main(String[] args) {
        Conexion c = new Conexion();
        c.getConnection();
    }
}
