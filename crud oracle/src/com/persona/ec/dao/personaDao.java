/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.persona.ec.dao;
import java.sql.*;

import com.persona.ec.entity.persona;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author werne
 */
public class personaDao {

    private String mensaje = "";

    public String agregarPersona(Connection con, persona per) {
        PreparedStatement pst= null;
        String Sql ="insert into persona values(sec_codigolibros.nextval,?,?,?)";
        try{
            pst= con.prepareStatement(Sql);
            pst.setString(1, per.getNombres());
            pst.setString(2, per.getCorreo());
             pst.setString(3, per.getTelefono());
             mensaje="GUARDADO CORRECTAMENTE";
            pst.execute();
            
            pst.close();
        }catch(SQLException e){
            mensaje="No se pudo guardar \n"+e.getMessage();
        }
        return mensaje;
    }

    public String modificarPersona(Connection con, persona per) {
         PreparedStatement pst= null;
        String Sql ="Update persona set nombres=?,correo=?,teelfono=? where id_persona=?";
        try{
            pst= con.prepareStatement(Sql);
            pst.setString(1, per.getNombres());
            pst.setString(2, per.getCorreo());
             pst.setString(3, per.getTelefono());
               pst.setInt(4, per.getIdPersona());
             mensaje="Actualizado CORRECTAMENTE";
            pst.execute();
            
            pst.close();
        }catch(SQLException e){
            mensaje="No se pudo actualizar \n"+e.getMessage();
        }
        return mensaje;
    }

    public String eliminarPersona(Connection con, int id) {
        
         PreparedStatement pst= null;
        String Sql ="Delete from persona where id_persona=?";
        try{
            pst= con.prepareStatement(Sql);
        
               pst.setInt(1, id);
             mensaje="Eliminado CORRECTAMENTE";
            pst.execute();
            
            pst.close();
        }catch(SQLException e){
            mensaje="No se pudo eliminar \n"+e.getMessage();
        }
       
        return mensaje;
    }

    public void listarPersona(Connection con , JTable tabla) {
  DefaultTableModel model = (DefaultTableModel) tabla.getModel();
  while(model.getRowCount() > 0)
{
    model.removeRow(0);
}
  //DefaultTableModel model = (DefaultTableModel) tabla.getModel() ;
    String [] Columnas={"ID","NOMBRE","CORREO","TELEFONO"};
    String sql ="SELECT * from persona";
            String [] filas= new String[4];
            PreparedStatement st =null;
            ResultSet rs = null;
         try{
             st= con.prepareStatement(sql);
             rs= st.executeQuery();
           
             while (rs.next()){
                 for(  int i =0; i<4; i++){
                     filas[i]= rs.getString(i+1);
                   
                 }
                 model.addRow(filas);
            
             }
             tabla.setModel(model);
         }   catch(Exception e) {
             JOptionPane.showMessageDialog(null, "no se puede listar la tabla"+e.getMessage());
         }
    }

   
}
