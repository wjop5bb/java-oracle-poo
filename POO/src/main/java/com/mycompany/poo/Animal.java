package com.mycompany.poo;

public class Animal extends SerVivo {
	
	@Override
	public void comer() {
		System.out.println("El animal "+getIdentificador()+" desgarra a su presa y come ");
		
	}

	@Override
	public void dormir() {
		System.out.println("El animal "+getIdentificador()+" se recuesta en el suelo y duerme ");
		
	}

}
