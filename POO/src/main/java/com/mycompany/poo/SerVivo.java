package com.mycompany.poo;

public abstract class SerVivo {
	private static final Integer NRO_CLASE = 1;
	private String identificador = null;
	private Integer estaturaCentimetros;
	private int estaturaCentimetrosInt;
	public Integer estaturaMetros;
	
	public void existir() {
		System.out.println("El Ser vivo "+identificador+" existe ");
		MiInterna mi = new MiInterna();
		mi.att = "";
		mi.att2= "";
	}
	
	public abstract void comer();
	
	public abstract void dormir();

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public Integer getEstaturaCentimetros() {
		return estaturaCentimetros;
	}

	public void setEstaturaCentimetros(Integer estaturaCentimetros) {
		this.estaturaCentimetros = estaturaCentimetros;
	}
	
	class MiInterna {
		private String att;
		protected String att2;
	}
	
	
	

}
