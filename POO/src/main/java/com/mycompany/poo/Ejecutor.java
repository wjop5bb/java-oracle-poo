package com.mycompany.poo;

public class Ejecutor {
	
	public static void main(String args[]) {

		SerVivo serVivo1 = new Persona();
		serVivo1.setIdentificador("Federico");
		serVivo1.setEstaturaCentimetros(155);
		serVivo1.estaturaMetros = 155;
		serVivo1.existir();
		serVivo1.comer();
		serVivo1.dormir();
		
		
		serVivo1 = new Animal();
		serVivo1.setIdentificador("Leon");
		serVivo1.setEstaturaCentimetros(60);
		serVivo1.existir();
		serVivo1.comer();
		serVivo1.dormir();
	}

}
