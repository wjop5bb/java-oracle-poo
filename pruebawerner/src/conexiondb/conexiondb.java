/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexiondb;
import java.sql.*;
import javax.swing.*;
/**
 *
 * @author werne
 */
public class conexiondb {
    private static Connection conn =null;
    private static String url ="jdbc:oracle:thin:@localhost:1521:xe";
    private static String pass = "oracle";
    private static String user ="system";
   
    
    public static Connection getConnection(){
        try{
                    Class.forName("oracle.jdbc.driver.OracleDriver");
                    conn=DriverManager.getConnection(url,user,pass);
                    conn.setAutoCommit(false);;
                    if(conn!=null){
                        System.out.println("Conexión satisfactoria");
                    }else{
                        System.out.println("error");
                    }
                    
        }catch (ClassNotFoundException | SQLException e) {
            JOptionPane.showMessageDialog(null, "Conexión erronea" + e.getMessage());
        }

        return conn;
    }
    
    public static void main (String[] args){
        conexiondb c = new conexiondb();
        c.getConnection();
        
        
    }
}
